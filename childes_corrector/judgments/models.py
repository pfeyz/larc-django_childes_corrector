from django.db import models
from django.contrib.auth.models import User

from corrections.models import UtteranceCorrection

class ErrorType(models.Model):
    name = models.CharField(max_length=32)
    description = models.TextField()

    def __unicode__(self):
        return self.name

class JudgmentLock(models.Model):
    correction = models.ForeignKey(UtteranceCorrection,
                                   null=False,
                                   blank=False,
                                   editable=False)
    user = models.ForeignKey(User, blank=False, null=False)
    created = models.DateTimeField(auto_now_add=True, blank=True)

    def __unicode__(self):
        return '{0} on {1} at {2}'.format(self.user, self.correction,
                                          self.created)

# TODO: ModifiedUtteranceCorrection should be merged into Judgment, and Judgment
# should have notes and replacement fields added and only valid when
# approved=False.

class Judgment(models.Model):
    correction = models.ForeignKey(UtteranceCorrection,
                                   null=False,
                                   blank=False,
                                   editable=False)
    error_type = models.ForeignKey(ErrorType, null=False, blank=False)
    author = models.ForeignKey(User, blank=False, null=False)
    approved = models.BooleanField(null=False)
    created = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    error = models.CharField(max_length=128, default='', blank=True)
    replacement = models.CharField(max_length=128, default='', blank=True)
    notes = models.TextField(blank=True, default='')
