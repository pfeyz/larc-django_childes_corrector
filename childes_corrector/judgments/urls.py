from django.conf.urls import patterns, include, url

from .views import JudgmentDetailView

urlpatterns = patterns('',
    url(r'^judgment/(?P<pk>\d+)$',
        JudgmentDetailView.as_view(), name='judgment_detail'),
)
