from django import forms
from django.views.generic import DetailView
from django.core.urlresolvers import reverse
from django.contrib import messages
from django.shortcuts import render_to_response, redirect
from django.template import RequestContext

import re
from corrections.models import UtteranceCorrection
from .models import ErrorType, JudgmentLock, Judgment
from .forms import JudgmentForm
from pprint import pprint

class JudgmentDetailView(DetailView):
    model = Judgment
    context_object_name = 'judgment'

    def get_context_data(self, **kwargs):
        context = super(JudgmentDetailView, self).get_context_data(**kwargs)
        obj = self.object
        mortier, target = find_error_range(obj.correction.error,
                                           obj.correction.utterance.mor_tier)
        context['target_index'] = target
        context['mortier'] = mortier
        return context

def find_error_range(error, mor_tier):
    matches = re.finditer(r'\b{0}\b'.format(
            re.escape(error)), mor_tier)
    if not matches:
        return None
    targets = []
    utterance = []
    matches = list(matches)
    last = 0
    for match in matches:
        pre = mor_tier[last:match.start()].split(' ')
        utterance.extend([p for p in pre if len(p) > 0])
        utterance.append(match.group(0))
        targets.append(len(utterance) - 1)
        last = match.end()
        #pprint([[i, w] for i, w in enumerate(utterance)])
        #print targets
    utterance.extend(mor_tier[last:].split(' '))
    return utterance, targets


def create_judgement(request, correction_id):
    default_error = ErrorType.objects.get(name='tagger error')
    correction = UtteranceCorrection.objects.get(pk=correction_id)
    if request.user.is_authenticated():
        JudgmentLock.objects.filter(user=request.user).delete()
        JudgmentLock(correction=correction, user=request.user).save()
    if request.method == 'GET':
        form = JudgmentForm(initial={'error_type': default_error,
                                     'approved': True})
    if request.method == 'POST':
        form = JudgmentForm(request.POST)
        next_id = UtteranceCorrection.next_unchecked(int(correction_id)).pk
        if 'skip_correction' in request.POST:
            next = reverse('judge_correction',
                           kwargs={'correction_id': next_id})
            return redirect(next)

        if not request.user.is_authenticated():
            messages.info(
                request,
                'You need to login to make any changes')
        elif 'edit_correction' in request.POST:
            return redirect(reverse('edit_correction',
                                    kwargs={'correction_id': int(correction_id)}))
        elif form.is_valid():
            judgment = form.instance
            judgment.author = request.user
            judgment.correction = correction
            judgment.save()
            next = reverse('judge_correction',
                           kwargs={'correction_id': next_id})
            return redirect(next)

    mortier, target = find_error_range(correction.error, correction.utterance.mor_tier)
    if target is not None:
        target = [x + 1 for x in target]

    form.fields['approved'].widget = forms.HiddenInput()
    form.fields['error'].widget = forms.HiddenInput()
    form.fields['replacement'].widget = forms.HiddenInput()
    form.fields['notes'].widget = forms.HiddenInput()
    return render_to_response('judgments/judgment_form.html',
                              {'form': form,
                               'correction': correction,
                               'target_index': target,
                               'mortier': mortier},
                              context_instance=RequestContext(request))

def edit_correction(request, correction_id):
    correction = UtteranceCorrection.objects.get(pk=int(correction_id))
    JudgmentLock.objects.filter(user=request.user).delete()
    JudgmentLock(correction=correction, user=request.user).save()
    correction.status = 's'
    correction.save()
    default_error = ErrorType.objects.get(name='tagger error')
    if request.method == 'GET':
        form = JudgmentForm(initial={
                'error_type': default_error,
                'approved': False,
                'correction': correction.error,
                'error': correction.error,
                'replacement': correction.replacement,
                'notes': correction.notes,
                })
    if request.method == 'POST':
        form = JudgmentForm(request.POST)
        form.fields['error'].required = True
        form.fields['replacement'].required = True
        if form.is_valid():
            edit = form.instance
            edit.author = request.user
            edit.correction = correction
            edit.save()
            correction.status = 'd'
            correction.save()
            next_id = UtteranceCorrection.next_unchecked(int(correction_id)).pk
            next = reverse('judge_correction',
                           kwargs={'correction_id': next_id})
            return redirect(next)

    mortier, target = find_error_range(correction.error,
                                       correction.utterance.mor_tier)
    if target is not None:
        target = [x + 1 for x in target]
    form.fields['approved'].widget = forms.HiddenInput()
    return render_to_response('judgments/edit_correction_form.html',
                              {'form': form, 'correction': correction,
                               'mortier': mortier,
                               'target_index': target },
                              context_instance=RequestContext(request))
