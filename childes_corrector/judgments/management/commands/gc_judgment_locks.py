from datetime import datetime, timedelta
from django.core.management.base import BaseCommand

from judgments.models import JudgmentLock

class Command(BaseCommand):
    help = 'Remove Judgment locks from the DB older than n minutes old'

    def handle(self, *args, **options):
        minutes = 10
        if args:
            minutes = int(args[0])
        too_old = (datetime.now() - timedelta(minutes=minutes))
        targets = JudgmentLock.objects.filter(created__lte=too_old)
        if targets:
            self.stdout.write('deleting ' + str(targets))
            targets.delete()
