from childes_corrector.settings.base import LOCK_TIMEOUT_MINUTES
from datetime import timedelta
from django.utils.timezone import now
from .models import JudgmentLock

class JudgementLockMiddleware(object):

    def process_request(self, request):
        """  """
        if request.user.is_authenticated():
            cutoff = now() - timedelta(seconds=(LOCK_TIMEOUT_MINUTES * 60))
            JudgmentLock.objects.filter(created__lte=cutoff).delete()
