# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Judgment.created'
        db.add_column(u'judgments_judgment', 'created',
                      self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, null=True, blank=True),
                      keep_default=False)

        # Adding field 'ModifiedUtteranceCorrection.created'
        db.add_column(u'judgments_modifiedutterancecorrection', 'created',
                      self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, null=True, blank=True),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'Judgment.created'
        db.delete_column(u'judgments_judgment', 'created')

        # Deleting field 'ModifiedUtteranceCorrection.created'
        db.delete_column(u'judgments_modifiedutterancecorrection', 'created')


    models = {
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'corrections.correctiondocument': {
            'Meta': {'ordering': "['source_file']", 'object_name': 'CorrectionDocument'},
            'author_initials': ('django.db.models.fields.CharField', [], {'max_length': '8'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'source_file': ('django.db.models.fields.FilePathField', [], {'path': "'/home/paul/repos/lab/django_childes_corrector_project/childes_corrector/data/corrections'", 'max_length': '512', 'recursive': 'True'}),
            'target_transcript': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['transcripts.Transcript']"})
        },
        u'corrections.utterancecorrection': {
            'Meta': {'ordering': "['utterance__transcript__name', 'utterance__uid']", 'object_name': 'UtteranceCorrection'},
            'correction_document': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['corrections.CorrectionDocument']"}),
            'error': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'notes': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'replacement': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'utterance': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['transcripts.Utterance']"})
        },
        u'judgments.errortype': {
            'Meta': {'object_name': 'ErrorType'},
            'description': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'})
        },
        u'judgments.judgment': {
            'Meta': {'object_name': 'Judgment'},
            'approved': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'author': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']"}),
            'correction': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['corrections.UtteranceCorrection']"}),
            'created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'null': 'True', 'blank': 'True'}),
            'error_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['judgments.ErrorType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'judgments.judgmentlock': {
            'Meta': {'object_name': 'JudgmentLock'},
            'correction': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['corrections.UtteranceCorrection']"}),
            'created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']"})
        },
        u'judgments.modifiedutterancecorrection': {
            'Meta': {'object_name': 'ModifiedUtteranceCorrection'},
            'author': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']"}),
            'correction': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['corrections.UtteranceCorrection']"}),
            'created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'null': 'True', 'blank': 'True'}),
            'error': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'error_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['judgments.ErrorType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'notes': ('django.db.models.fields.TextField', [], {'default': "''", 'blank': 'True'}),
            'replacement': ('django.db.models.fields.CharField', [], {'max_length': '128'})
        },
        u'transcripts.corpus': {
            'Meta': {'object_name': 'Corpus'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '128'})
        },
        u'transcripts.transcript': {
            'Meta': {'ordering': "['name']", 'unique_together': "(['corpus', 'name'],)", 'object_name': 'Transcript'},
            'corpus': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['transcripts.Corpus']"}),
            'footers': ('django.db.models.fields.TextField', [], {}),
            'headers': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.TextField', [], {}),
            'source_file': ('django.db.models.fields.FilePathField', [], {'path': "'/home/paul/repos/lab/django_childes_corrector_project/childes_corrector/data/transcripts'", 'max_length': '512', 'recursive': 'True'})
        },
        u'transcripts.utterance': {
            'Meta': {'ordering': "['uid']", 'unique_together': "(['transcript', 'uid'],)", 'object_name': 'Utterance'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'main_tier': ('django.db.models.fields.TextField', [], {}),
            'mor_tier': ('django.db.models.fields.TextField', [], {}),
            'speaker': ('django.db.models.fields.CharField', [], {'max_length': '16'}),
            'transcript': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['transcripts.Transcript']"}),
            'uid': ('django.db.models.fields.IntegerField', [], {})
        }
    }

    complete_apps = ['judgments']