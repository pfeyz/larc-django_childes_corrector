from django.forms import ModelForm

from .models import Judgment

class JudgmentForm(ModelForm):
    class Meta:
        model = Judgment
        exclude = ('author',)
