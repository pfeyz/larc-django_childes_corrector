from django.core.management.base import BaseCommand, CommandError
from glob import glob

from childes_corrector.settings.base import project_file
from transcripts.models import Transcript

class Command(BaseCommand):
    help = 'Load the transcripts from the data dir into the db'

    def handle(self, *args, **options):
        if args:
            fns = args
        else:
            fns = glob(project_file('data', 'transcripts', '*/*'))
            fns = sorted(fns)
        for fn in fns:
            self.stdout.write('reading ' + fn)
            Transcript.from_file(fn)
            self.stdout.write('done')
