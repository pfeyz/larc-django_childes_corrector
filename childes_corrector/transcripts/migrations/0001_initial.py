# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Utterance'
        db.create_table(u'transcripts_utterance', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('transcript', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['transcripts.Transcript'])),
            ('uid', self.gf('django.db.models.fields.IntegerField')()),
            ('speaker', self.gf('django.db.models.fields.CharField')(max_length=16)),
            ('main_tier', self.gf('django.db.models.fields.TextField')()),
            ('mor_tier', self.gf('django.db.models.fields.TextField')()),
        ))
        db.send_create_signal(u'transcripts', ['Utterance'])

        # Adding unique constraint on 'Utterance', fields ['transcript', 'uid']
        db.create_unique(u'transcripts_utterance', ['transcript_id', 'uid'])

        # Adding model 'Corpus'
        db.create_table(u'transcripts_corpus', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(unique=True, max_length=128)),
        ))
        db.send_create_signal(u'transcripts', ['Corpus'])

        # Adding model 'Transcript'
        db.create_table(u'transcripts_transcript', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('corpus', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['transcripts.Corpus'])),
            ('name', self.gf('django.db.models.fields.TextField')()),
            ('headers', self.gf('django.db.models.fields.TextField')()),
            ('footers', self.gf('django.db.models.fields.TextField')()),
            ('source_file', self.gf('django.db.models.fields.FilePathField')(path='/home/paul/repos/lab/django_childes_corrector_project/data/transcripts', max_length=100, recursive=True)),
        ))
        db.send_create_signal(u'transcripts', ['Transcript'])

        # Adding unique constraint on 'Transcript', fields ['corpus', 'name']
        db.create_unique(u'transcripts_transcript', ['corpus_id', 'name'])


    def backwards(self, orm):
        # Removing unique constraint on 'Transcript', fields ['corpus', 'name']
        db.delete_unique(u'transcripts_transcript', ['corpus_id', 'name'])

        # Removing unique constraint on 'Utterance', fields ['transcript', 'uid']
        db.delete_unique(u'transcripts_utterance', ['transcript_id', 'uid'])

        # Deleting model 'Utterance'
        db.delete_table(u'transcripts_utterance')

        # Deleting model 'Corpus'
        db.delete_table(u'transcripts_corpus')

        # Deleting model 'Transcript'
        db.delete_table(u'transcripts_transcript')


    models = {
        u'transcripts.corpus': {
            'Meta': {'object_name': 'Corpus'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '128'})
        },
        u'transcripts.transcript': {
            'Meta': {'ordering': "['name']", 'unique_together': "(['corpus', 'name'],)", 'object_name': 'Transcript'},
            'corpus': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['transcripts.Corpus']"}),
            'footers': ('django.db.models.fields.TextField', [], {}),
            'headers': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.TextField', [], {}),
            'source_file': ('django.db.models.fields.FilePathField', [], {'path': "'/home/paul/repos/lab/django_childes_corrector_project/data/transcripts'", 'max_length': '100', 'recursive': 'True'})
        },
        u'transcripts.utterance': {
            'Meta': {'ordering': "['uid']", 'unique_together': "(['transcript', 'uid'],)", 'object_name': 'Utterance'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'main_tier': ('django.db.models.fields.TextField', [], {}),
            'mor_tier': ('django.db.models.fields.TextField', [], {}),
            'speaker': ('django.db.models.fields.CharField', [], {'max_length': '16'}),
            'transcript': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['transcripts.Transcript']"}),
            'uid': ('django.db.models.fields.IntegerField', [], {})
        }
    }

    complete_apps = ['transcripts']