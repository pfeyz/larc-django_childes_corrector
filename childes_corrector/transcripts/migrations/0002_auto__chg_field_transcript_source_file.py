# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):

        # Changing field 'Transcript.source_file'
        db.alter_column(u'transcripts_transcript', 'source_file', self.gf('django.db.models.fields.FilePathField')(path='/home/pfeyz/webapps/childes_corrector/childes_corrector_project/data/transcripts', max_length=512, recursive=True))

    def backwards(self, orm):

        # Changing field 'Transcript.source_file'
        db.alter_column(u'transcripts_transcript', 'source_file', self.gf('django.db.models.fields.FilePathField')(path='/home/paul/repos/lab/django_childes_corrector_project/data/transcripts', max_length=100, recursive=True))

    models = {
        u'transcripts.corpus': {
            'Meta': {'object_name': 'Corpus'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '128'})
        },
        u'transcripts.transcript': {
            'Meta': {'ordering': "['name']", 'unique_together': "(['corpus', 'name'],)", 'object_name': 'Transcript'},
            'corpus': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['transcripts.Corpus']"}),
            'footers': ('django.db.models.fields.TextField', [], {}),
            'headers': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.TextField', [], {}),
            'source_file': ('django.db.models.fields.FilePathField', [], {'path': "'/home/pfeyz/webapps/childes_corrector/childes_corrector_project/data/transcripts'", 'max_length': '512', 'recursive': 'True'})
        },
        u'transcripts.utterance': {
            'Meta': {'ordering': "['uid']", 'unique_together': "(['transcript', 'uid'],)", 'object_name': 'Utterance'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'main_tier': ('django.db.models.fields.TextField', [], {}),
            'mor_tier': ('django.db.models.fields.TextField', [], {}),
            'speaker': ('django.db.models.fields.CharField', [], {'max_length': '16'}),
            'transcript': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['transcripts.Transcript']"}),
            'uid': ('django.db.models.fields.IntegerField', [], {})
        }
    }

    complete_apps = ['transcripts']