from datetime import timedelta
from django.db.models import Count
from django.views.generic import ListView, DetailView
from django.contrib.auth.models import User

from .models import Transcript
from corrections.models import UtteranceCorrection

class TranscriptListView(ListView):
    model = Transcript
    context_object_name = 'transcript_list'
    leader_board = False

    def get_context_data(self, *args, **kwargs):
        context = super(TranscriptListView, self).get_context_data(*args,
                                                                    **kwargs)
        context['total_corrections'] = UtteranceCorrection.objects.count()
        context['total_unchecked'] = UtteranceCorrection.all_unchecked().count()
        user_list = User.objects.annotate(
            num_judgments=Count('judgment'),
            ).order_by('-num_judgments')
        if self.leader_board:
            for user in user_list:
                js = user.judgment_set.exclude(created=None).order_by('created')
                deltas = [(js[i+1].created - js[i].created).total_seconds() for i in range(len(js) -1)]
                s, sessions = 0, []
                for d in deltas:
                    if d > 60 * 10:
                        sessions.append(s)
                        s = 0
                    else:
                        s += 1
                sessions.append(s)
                deltas = filter(lambda x: x < 60 * 10, deltas)
                if deltas:
                    user.time = int(sum(deltas) / len(deltas))
                else:
                    user.time = 'NA'
                user.sessions = sum(sessions) / len(sessions)
        context['leader_board'] = self.leader_board
        context['user_list'] = user_list
        return context

class TranscriptDetailView(DetailView):
    model = Transcript
