import re

from django.db import models
from childes_corrector.settings.base import project_file
from os import path

def strip_speaker(s):
    return s.lstrip('*').rstrip(':')

class Utterance(models.Model):
    transcript = models.ForeignKey('Transcript')
    uid = models.IntegerField()
    speaker = models.CharField(max_length=16)
    main_tier = models.TextField()
    mor_tier = models.TextField()

    class Meta:
        ordering = ['uid']
        unique_together = ['transcript', 'uid']

    def __unicode__(self):
        return '{0}:{1}'.format(self.transcript,
                                self.uid)

class Corpus(models.Model):
    name = models.CharField(max_length=128, unique=True)

    class Meta:
        verbose_name_plural = "corpora"

    def __unicode__(self):
        return self.name

class Transcript(models.Model):
    corpus = models.ForeignKey(Corpus)
    name = models.TextField()
    headers = models.TextField()
    footers = models.TextField()
    source_file = models.FilePathField(
        max_length=512,
        path=project_file('data', 'transcripts'),
        recursive=True)

    class Meta:
        ordering = ['name']
        unique_together = ['corpus', 'name']

    def next_unchecked_correction(self):
        from corrections.models import UtteranceCorrection
        return UtteranceCorrection.next_unchecked(transcript=self)

    def corrections(self):
        from corrections.models import UtteranceCorrection
        return UtteranceCorrection.objects.filter(utterance__transcript=self)

    def unchecked_utterances(self):
        from corrections.models import UtteranceCorrection
        return UtteranceCorrection.all_unchecked().filter(
            utterance__transcript=self)

    def judgments(self):
        from judgments.models import Judgment
        return Judgment.objects.filter(
            correction__utterance__transcript=self)

    @classmethod
    def from_file(cls, filename):
        rest, name = path.split(filename)
        rest, corpus = path.split(rest)
        with open(filename) as fh:
            lines = fh.readlines()
        headers = cls.get_headers(lines)
        footers = cls.get_footers(lines)

        corpus, _ = Corpus.objects.get_or_create(name=corpus)
        transcript = Transcript(
            corpus=corpus,
            name=name,
            headers='\n'.join(list(headers)),
            footers='\n'.join(list(footers)),
            source_file=filename)
        transcript.save()
        utterances = []
        for uid, (speaker, main, mor) in enumerate(cls.get_tiers(lines)):
            utterances.append(Utterance(transcript=transcript,
                                  uid=uid,
                                  speaker=speaker,
                                  main_tier=main,
                                  mor_tier=mor or ''))
        Utterance.objects.bulk_create(utterances)
        return transcript

    @staticmethod
    def get_headers(lines):
        for line in lines:
            if line.startswith('@'):
                yield line.strip()
            else:
                break

    @staticmethod
    def get_footers(lines):
        footers = []
        for line in lines[::-1]:
            if line.startswith('@'):
                footers.append(line.strip())
            else:
                break
        return footers[::-1]

    @staticmethod
    def get_tiers(lines):
        speaker = None
        main, mor = None, None  # tiers
        for line in lines:
            #logger.debug('parsing line {0}'.format(line.__repr__()))
            line = re.sub(r'(\s+)', r'\1', line)
            line = line.strip()
            if line.startswith('*'):
                if speaker:  # yield previous line
                    yield strip_speaker(speaker) , main, mor
                speaker, main =  line.split('\t')
                mor = None
            elif line.startswith('%mor:'):
                _, mor =  line.rstrip().split('\t')
        yield strip_speaker(speaker), main, mor

    def __unicode__(self):
        rest, fn = path.split(self.source_file)
        rest, corpus = path.split(rest)
        return '{0}/{1}'.format(corpus, fn)
