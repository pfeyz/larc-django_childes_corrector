import random
from django.test import TestCase
from django.test.client import Client
from django.contrib.auth.models import User
from corrections.models import UtteranceCorrection
from transcripts.models import Transcript

class CorrectionTests(TestCase):
    fixtures = ['single_doc', 'error_types']

    def setUp(self):
        self.assertNotEqual(0, UtteranceCorrection.objects.count(),
                           'fixtures failed to load')
        self.assertNotEqual(0, Transcript.objects.count(),
                           'fixtures failed to load')

    def test_give_auth_users_unique_corrections(self):
        clients = [Client() for _ in range(3)]
        users = [User.objects.
                 create_user('user %s' % i, '', 'test') for i in range(3)]
        for client, user in zip(clients, users):
            user.save()
            client.login(username=user.username, password='test')

        corrections = {}
        for _ in UtteranceCorrection.objects.all():
            client = random.choice(clients)
            correction = UtteranceCorrection.next_unchecked()
            corrections[client] = correction
            client.get(correction.get_absolute_url())
            c = corrections.values()
            self.assertEqual(len(c), len(set(c)))
