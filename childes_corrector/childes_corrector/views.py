from django.views.generic import ListView
from django.shortcuts import get_object_or_404
from django.contrib.auth.models import User
from django.contrib import messages
from judgments.models import Judgment
from django.shortcuts import redirect
from django.contrib.auth import authenticate
from django.contrib.auth import login as auth_login
from django.contrib.auth import logout as auth_logout

class UserActivityListView(ListView):
    template_name = 'users/user_activity.html'
    context_object_name = 'judgment_list'
    paginate_by = 40
    def get_queryset(self, *args, **kwargs):
        name = self.kwargs.get('username')
        return Judgment.objects.filter(author__username=name).exclude(
            created=None).order_by('-created')

    def get_context_data(self, *args, **kwargs):
        context = super(UserActivityListView, self).get_context_data(*args, **kwargs)
        context['target_user'] = get_object_or_404(User, username=self.kwargs['username'])
        return context

class AllActivityListView(ListView):
    template_name = 'users/all_activity.html'
    queryset = Judgment.objects.exclude(created=None).order_by('-created')
    context_object_name = 'judgment_list'
    paginate_by = 40

def login(request):
    username = request.POST['username']
    password = request.POST['password']
    user = authenticate(username=username, password=password)
    if user is not None and user.is_active:
        auth_login(request, user)
    else:
        messages.add_message(request, messages.WARNING, 'Bad username/password.')
    return redirect(request.POST.get('came_from', '/'))

def logout(request):
    auth_logout(request)
    return redirect('/')
