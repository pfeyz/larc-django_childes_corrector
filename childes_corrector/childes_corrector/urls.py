
from django.conf.urls import patterns, include, url

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

from childes_corrector.views import (UserActivityListView, AllActivityListView,
                                     login, logout)
from corrections.views import UtteranceCorrectionListView, next_unchecked
from transcripts.views import TranscriptListView, TranscriptDetailView
from judgments.views import create_judgement, edit_correction

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'childes_corrector.views.home', name='home'),
    # url(r'^childes_corrector/', include('childes_corrector.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^$', TranscriptListView.as_view(), name='home'),
    url(r'^correction/next',
        next_unchecked, name='next_correction'),
    url(r'^correction/(?P<correction_id>\d+)$',
        create_judgement, name='judge_correction'),
    url(r'^correction/(?P<correction_id>\d+)/edit$',
        edit_correction, name='edit_correction'),
    url(r'^corrections/(?P<transcript>\d+)$',
        UtteranceCorrectionListView.as_view(), name='correction_list'),
    url(r'^transcript/(?P<pk>\d+)$',
        TranscriptDetailView.as_view(), name='view_transcript'),
    url(r'^login/$', login, name='login'),
    url(r'^logout/$', logout, name='logout'),
    url(r'^user/(?P<username>.+?)/activity$',
        UserActivityListView.as_view(), name='user_activity'),
    url(r'^activity$',
        AllActivityListView.as_view(), name='all_activity'),
    url(r'^', include('judgments.urls')),
    url(r'^admin/', include(admin.site.urls)),
)
