from .base import *
from os.path import expanduser

DEBUG = False
TEMPLATE_DEBUG = DEBUG

ALLOWED_HOSTS = ['childes-corex.pfeyz.webfactional.com']
STATIC_URL = 'http://childes-corex.pfeyz.webfactional.com/static/'
STATIC_ROOT = expanduser('~/webapps/childes_corrector_static')
