from django.contrib import admin

from corrections.models import CorrectionDocument, UtteranceCorrection
from transcripts.models import Transcript, Utterance, Corpus
from judgments.models import ErrorType, Judgment, JudgmentLock

class JudgmentAdmin(admin.ModelAdmin):
    list_display = ['correction', 'author', 'approved', 'error_type', 'created']

class LockAdmin(admin.ModelAdmin):
    list_display = ['correction', 'user', 'created']

class ErrorTypeAdmin(admin.ModelAdmin):
    list_display = ['name', 'description']

admin.site.register(Judgment, JudgmentAdmin)
admin.site.register(JudgmentLock, LockAdmin)
admin.site.register(ErrorType, ErrorTypeAdmin)

for model in (Transcript, Utterance, CorrectionDocument,
              UtteranceCorrection, Corpus):
    admin.site.register(model)
