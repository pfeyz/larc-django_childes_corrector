import csv
import logging
import re
from os import path
from django.db import models

from childes_corrector.settings.base import project_file
from transcripts.models import Transcript, Utterance

sniffer = csv.Sniffer()
logger = logging.getLogger(__name__)

def all_blank(line):
    return all(map(lambda x: x.isspace() or not x, line))

FILENAME_REGEX = re.compile(r"""(\w+?) # corpus name
                                (\d+\w?) # Transcript ID
                                (_
                                (\w\w))? # Author initials
                             """,
                            flags=re.X)

class CorrectionDocument(models.Model):
    target_transcript = models.ForeignKey(Transcript)
    author_initials = models.CharField(max_length=8)
    source_file = models.FilePathField(
        path=project_file('data', 'corrections'),
        max_length=512,
        recursive=True)

    class Meta:
        ordering = ['source_file']

    def __unicode__(self):
        return path.basename(self.source_file)

    @classmethod
    def from_file(cls, correction_csv_filename):
        """ Reads a utterance correction csv file and returns a dict of
        {uid: [UtteranceCorrector]} key/value pairs.

        They keys are the utterance id targeted for correction and the
        values are lists of UtteranceCorrector objects (multiple
        corrections can target a single utterance.)

        the csv should be in the following format:

          Utterance ID, Speaker ID, Main Tier, MOR Tier, Observed Error, Correction, Notes

        here's an example row:

         "150", "*MOT:", "no , what ?", "qn|no , pro:wh|what ?", "qn|no", "co|no",


        """

        match = FILENAME_REGEX.search(correction_csv_filename)
        corpus_name = match.group(1).lower()
        transcript_name = match.group(2)
        transcript_name = '{0}.longtr.cex'.format(transcript_name)
        author_initials = match.group(3) or ''
        # alt_transcript_name = '{0}.cha'.format(transcript_name)
        document = csv.reader(open(correction_csv_filename))
        start_line = 0
        if sniffer.has_header(correction_csv_filename):
            # logger.debug('headers detected and skipped in {0}'.format(
            #         correction_csv_filename))
            document.next()
            start_line = 1

        correction_doc = CorrectionDocument(
            source_file=correction_csv_filename,
            target_transcript=Transcript.objects.get(
                name=transcript_name,
                corpus__name=corpus_name),
            author_initials = author_initials)
        correction_doc.save()
        corrections = []
        for line_num, line in enumerate(document):
            line_num += 1 + start_line
            if all_blank(line):
                logger.warning('blank line in {0}:{1}'.format(
                        correction_csv_filename, line_num))
                continue
            logger.debug('creating correction from {0}'.format(line))
            error_msg = 'non-blank trailing columns in correction {0}:{1}'.format(
                correction_csv_filename, line_num)
            try:
                uid, speaker_id, main, mor, error, replacement, notes = line[:7]
                if not all_blank(line[7:]):
                    logger.warning('{0}:{1}'.format(error_msg, line[7:]))
            except ValueError:
                uid, speaker_id, main, mor, error, replacement = line[:6]
                notes = ''
                if not all_blank(line[6:]):
                    logger.warning('{0}:{1}'.format(error_msg, line[6:]))
            uid = int(uid)
            utterance = Utterance.objects.get(
                transcript=correction_doc.target_transcript,
                uid=uid)
            corrections.append(UtteranceCorrection(
                    correction_document=correction_doc,
                    utterance=utterance,
                    error=error,
                    replacement=replacement,
                    notes=notes))
        UtteranceCorrection.objects.bulk_create(corrections)
        return corrections

class UtteranceCorrection(models.Model):
    correction_document = models.ForeignKey(CorrectionDocument)
    utterance = models.ForeignKey(Utterance, editable=False)
    error = models.CharField(max_length=128)
    replacement = models.CharField(max_length=128)
    notes = models.TextField(blank=True)

    class Meta:
        ordering = ['utterance__transcript__name', 'utterance__uid']

    def __unicode__(self):
        return '{0}:{1}'.format(self.utterance.transcript, self.utterance.uid,
                                    self.pk)

    @classmethod
    def all_unchecked(cls):
        return cls.objects.filter(judgment=None, judgmentlock=None)

    @classmethod
    def next_unchecked(cls, start_from=None, transcript=None):
        queryset = cls.all_unchecked()
        args = {}
        if transcript:
            args['utterance__transcript'] = transcript
        if start_from:
            obj = cls.objects.get(pk=start_from)
            args['utterance__transcript__name__gte'] = \
                obj.utterance.transcript.name
            args['utterance__uid__gt'] = obj.utterance.uid
            q = queryset.filter(**args)
            if not q.count():
                del args['utterance__uid__gt']
                del args['utterance__transcript__name__gte']
                args['utterance__transcript__name__gt'] = \
                    obj.utterance.transcript.name
                q = queryset.filter(**args)
            return q[0]
        return queryset.filter(**args)[0]
