# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'CorrectionDocument'
        db.create_table(u'corrections_correctiondocument', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('target_transcript', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['transcripts.Transcript'])),
            ('author_initials', self.gf('django.db.models.fields.CharField')(max_length=8)),
            ('source_file', self.gf('django.db.models.fields.FilePathField')(path='/home/paul/repos/lab/django_childes_corrector_project/data/corrections', max_length=100, recursive=True)),
        ))
        db.send_create_signal(u'corrections', ['CorrectionDocument'])

        # Adding model 'UtteranceCorrection'
        db.create_table(u'corrections_utterancecorrection', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('correction_document', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['corrections.CorrectionDocument'])),
            ('utterance', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['transcripts.Utterance'])),
            ('error', self.gf('django.db.models.fields.CharField')(max_length=128)),
            ('replacement', self.gf('django.db.models.fields.CharField')(max_length=128)),
            ('notes', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('status', self.gf('django.db.models.fields.CharField')(default='u', max_length=1)),
        ))
        db.send_create_signal(u'corrections', ['UtteranceCorrection'])


    def backwards(self, orm):
        # Deleting model 'CorrectionDocument'
        db.delete_table(u'corrections_correctiondocument')

        # Deleting model 'UtteranceCorrection'
        db.delete_table(u'corrections_utterancecorrection')


    models = {
        u'corrections.correctiondocument': {
            'Meta': {'ordering': "['source_file']", 'object_name': 'CorrectionDocument'},
            'author_initials': ('django.db.models.fields.CharField', [], {'max_length': '8'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'source_file': ('django.db.models.fields.FilePathField', [], {'path': "'/home/paul/repos/lab/django_childes_corrector_project/data/corrections'", 'max_length': '100', 'recursive': 'True'}),
            'target_transcript': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['transcripts.Transcript']"})
        },
        u'corrections.utterancecorrection': {
            'Meta': {'ordering': "['utterance__transcript__name', 'utterance__uid']", 'object_name': 'UtteranceCorrection'},
            'correction_document': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['corrections.CorrectionDocument']"}),
            'error': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'notes': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'replacement': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'status': ('django.db.models.fields.CharField', [], {'default': "'u'", 'max_length': '1'}),
            'utterance': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['transcripts.Utterance']"})
        },
        u'transcripts.corpus': {
            'Meta': {'object_name': 'Corpus'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '128'})
        },
        u'transcripts.transcript': {
            'Meta': {'ordering': "['name']", 'unique_together': "(['corpus', 'name'],)", 'object_name': 'Transcript'},
            'corpus': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['transcripts.Corpus']"}),
            'footers': ('django.db.models.fields.TextField', [], {}),
            'headers': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.TextField', [], {}),
            'source_file': ('django.db.models.fields.FilePathField', [], {'path': "'/home/paul/repos/lab/django_childes_corrector_project/data/transcripts'", 'max_length': '100', 'recursive': 'True'})
        },
        u'transcripts.utterance': {
            'Meta': {'ordering': "['uid']", 'unique_together': "(['transcript', 'uid'],)", 'object_name': 'Utterance'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'main_tier': ('django.db.models.fields.TextField', [], {}),
            'mor_tier': ('django.db.models.fields.TextField', [], {}),
            'speaker': ('django.db.models.fields.CharField', [], {'max_length': '16'}),
            'transcript': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['transcripts.Transcript']"}),
            'uid': ('django.db.models.fields.IntegerField', [], {})
        }
    }

    complete_apps = ['corrections']