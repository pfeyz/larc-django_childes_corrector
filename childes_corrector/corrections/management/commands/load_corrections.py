from django.core.management.base import BaseCommand, CommandError
from glob import glob

from childes_corrector.settings.base import project_file
from corrections.models import CorrectionDocument

class Command(BaseCommand):
    help = 'Load the transcripts from the data dir into the db'

    def handle(self, *args, **options):
        if args:
            fns = args
        else:
            fns = glob(project_file('data', 'corrections', '*/*'))
        for fn in fns:
            self.stdout.write('reading ' + fn)
            CorrectionDocument.from_file(fn)
            self.stdout.write('done')
