from django.core.urlresolvers import reverse
from django.shortcuts import redirect
from django.views.generic import ListView

from .models import UtteranceCorrection
from judgments.models import JudgmentLock

class UtteranceCorrectionListView(ListView):
    model = UtteranceCorrection
    transcript = None

    def get(self, request, *args, **kwargs):
        if 'transcript' in kwargs:
            tid = kwargs['transcript']
            self.queryset = self.model.objects.filter(utterance__transcript=tid)
        return super(UtteranceCorrectionListView, self).get(
            request, *args, **kwargs)

def next_unchecked(request):
    kwargs = request.GET.get('start_from', {})
    transcript = request.GET.get('transcript', None)
    if request.user.is_authenticated():
        # this keeps the user from skipping over their own judgment lock. a
        # workaround for a poorly designed locking system.
        JudgmentLock.objects.filter(user=request.user).delete()
    if transcript:
        kwargs['transcript'] = transcript
    next_id = UtteranceCorrection.next_unchecked(**kwargs).pk
    return redirect(reverse('judge_correction',
                            kwargs={'correction_id': next_id}))
